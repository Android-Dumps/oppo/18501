#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:1ca0ff548777df2233369d7eeab34ff40818bd41; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:50cbd63d63512efcbc9c2171463f41cc56659b4d \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:1ca0ff548777df2233369d7eeab34ff40818bd41 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.recovery.updated true
fi
